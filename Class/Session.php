<?php

class Session
{

    public static function start($user_id)
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $_SESSION['last_trace'] = time();
        $_SESSION['user_id'] = $user_id;
    }

    public static function close()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        session_destroy();
    }

    /**
     * return true when login
     * 
     * @return type
     *
     */
    public static function status()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $expiryTime = 20000;
        if (isset($_SESSION['last_trace'])) {
            if ((int) $_SESSION['last_trace'] + $expiryTime < time()) {
                session_destroy();
                return false;
            }
            
            $_SESSION['last_trac'] = time();
            
            return true;
        } else {
            session_destroy();
        }
        return false;
    }
}