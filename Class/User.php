<?php
class User
{
    public $login;
    public $name;
    public $password;
    public $lastName;
    public $id;

    /**
     * sprawdza czy podane hasło i login są poprawne
     * zwraca -1 jeśli dane nie są poprawne
     * @return id int
     */
    public function loginCheck($login, $password)
    {
        $u=new UserDb();
        return $u->loginPassword($login, $password);
       
    }
}