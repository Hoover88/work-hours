<?php
class DayHours
{
    public $department;
    public $begin;
    public $end;
    public $desc;
    public $km;

    /**
     * zapisuje z danego dnia wszystkie godziny pracy, stare zostają usunięte
     *@param array DayHours 
     * 
     */
    function saveAll($DayHoursArray,$day, $month,$year)
    {
      $DB=new DayHoursDb();
      $DB->deleteDayHours($_SESSION['user_id'],$day, $month,$year);
      foreach($DayHoursArray as $DayHour)
      {
          //('Y-m-d H:i:s')
          $DayHour->begin=$year.'-'.$month.'-'.$day.' '.$DayHour->begin.':00'; 
          $DayHour->end=$year.'-'.$month.'-'.$day.' '.$DayHour->end.':00';
          $temp=new DepartmentsDb();
          $DayHour->department=$temp->getIdDepartment($DayHour->department); //TODO: przechwycić brak departamentu
          $DB->addHours($_SESSION['user_id'], $DayHour);
      }
    }
    
    
    
}