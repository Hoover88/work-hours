<?php

class AutenticationController
{

    public function login()
    {
        $template = $GLOBALS["twig"]->loadTemplate('login.twig');
        if (Session::status()) {
            // TODO: Jesteś już zalogowany
            echo "jeseś zalogowany";
        } else {
            
            if (isset($_POST["button"])) {
                if (isset($_POST["login"]) && $_POST["password"]) {
                    $u = new User();
                    $userId = $u->loginCheck($_POST["login"], $_POST["password"]);
                    if ($userId == - 1) {
                        echo $template->render(array(
                            'error' => 'niepoprawny login lub hasło',
                            ));
                       return;
                    } else {
                        Session::start($userId);
                        header( 'Location: http://'.$_SERVER['SERVER_NAME'] ) ;
                    }
                } else {
                    // TODO: prosze wpisać login i hasło
                }
            }
        }
        
       
        echo $template->render(array());
    }

    public function logout()
    {
        Session::close();
        //TODO: wyswietlenie logowania jeśli urzytkownik nie był zalogowany
        $template = $GLOBALS["twig"]->loadTemplate('logout.twig');
        echo $template->render(array());
    }
}
?>