<?php
class MainController
{
    public function main()
    {
        $temp =new DayHoursDb();
        $template = $GLOBALS["twig"]->loadTemplate('main.twig');
        echo $template->render(array( 
            'todayDate'=>date("d.F.Y"),
            'MonthHours'=>$temp->monthHours($_SESSION['user_id'],date('m'),date('Y')),
            'monthKm'=>$temp->monthKm($_SESSION['user_id'], date('m'), date('Y')) 
        ));
      
    }
    public function addHours($date)
    {
        
        $template = $GLOBALS["twig"]->loadTemplate('addHours.twig');
        
        $dep=new DepartmentsDb();
        $h=new DayHoursDb();
        $Y=explode('-', $date)[2];
        $m=explode('-', $date)[1];
        $d=explode('-', $date)[0];
        echo $template->render(array(
            'date'=>str_replace('.', '/', $date) ,
            'departments'=>$dep->getDepartments(),
            'hours'=>$h->getDayHours(2, $Y.'-'.$m.'-'.$d) //format YY-mm-dd
        ));
    }
    public function addHoursJon() //TODO: sprawdzić czy sesja jest aktywna
    {
        $jsonObj= json_decode(file_get_contents("php://input"));
        if(!isset($jsonObj))
        {
            echo "error";
            exit;
        }
        
        $DayHoursArray= array();
        foreach ($jsonObj->DayHours as $key=>$obj )
        {
            $DayHoursArray[$key]=new DayHours();
            $DayHoursArray[$key]->department = $obj->department ;
            $DayHoursArray[$key]->begin = $obj->begin;
            $DayHoursArray[$key]->end = $obj->end;
            $DayHoursArray[$key]->km = $obj->km;
        }
        $day=$jsonObj->day;
        $month=$jsonObj->month;
        $year=$jsonObj->year;
        
        $temp=new DayHours();
        $temp->saveAll($DayHoursArray,$day,$month,$year);
        
            echo json_encode (array());
       
    }
    
    public function raport()
    { 
        $template = $GLOBALS["twig"]->loadTemplate('raport.twig');
        
    }
}