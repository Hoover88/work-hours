<?php

class ErrorsController
{

    public function error404()
    {
            $template = $GLOBALS["twig"]->loadTemplate('error404.twig');
            
            echo $template->render(array());
    }
}