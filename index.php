<?php 
try{
include 'lib/Twig/Autoloader.php';
Twig_Autoloader::register();
//TWIG
$loader = new Twig_Loader_Filesystem('Views');
$GLOBALS["twig"] = new Twig_Environment($loader);

/**
 * Autoloader
 */
function classLoader($classname){
	$Class = $_SERVER['DOCUMENT_ROOT'].'/Class/'.$classname.'.php';
	if(file_exists($Class) && is_readable($Class) && !class_exists($classname, false))
	{
	    require_once($Class);
	    return true;
	}
	$Controller = $_SERVER['DOCUMENT_ROOT'].'/Controllers/'.$classname.'.php';
	if (file_exists($Controller) && is_readable($Controller) && !class_exists($classname, false))
	{
	    require_once($Controller);
	    return true;
	}
	$Entity = $_SERVER['DOCUMENT_ROOT'].'/Entity/'.$classname.'.php';
	if (file_exists($Entity) && is_readable($Entity) && !class_exists($classname, false))
	{
	    require_once($Entity);
	    return true;
	}
	return false;
}
spl_autoload_register('classLoader');
include_once 'routing.php';
}
catch (Exception $e)
{
    echo "wystapil blad";
}
?>