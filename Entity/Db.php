<?php

/**
 * klasa odpowiedzialna za incjowanie połączenia
 * @author xxx
 *
 */
class Db
{

    public $pdo;

    function __construct()
    {
        $ini_array = parse_ini_file($_SERVER['DOCUMENT_ROOT'] . '/config/config.ini', true); // TODO sprawdzić czy plik istnieje
        
        $host = $ini_array['database']['host'];
        $login = $ini_array['database']['login'];
        $password = $ini_array['database']['password'];
        $port = $ini_array['database']['port'];
        
        $this->pdo = new PDO('mysql:host=' . $host . ';dbname=work_hours;port=' . $port, $login, $password);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
}