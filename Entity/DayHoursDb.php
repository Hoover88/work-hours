<?php

/**
 * 
 * Klasa reprezentująca godziny urzytkownika w bazie danych
 *
 */
class DayHoursDb extends Db
{

    /**
     * Zwraca listę godzin pracownika w danym dniu
     *
     * @return array()
     */
    public function getDayHours($idUser, $dat)
    {
        $stmt = $this->pdo->prepare('SELECT name, begin, end, km FROM DayHours h join Departments d on h.idDepartment=d.id
             WHERE idUser = :idUser AND DATE(begin) =  :begin');
        
        $stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);
        $stmt->bindValue(':begin', $dat, PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        $tab = array();
        foreach ($result as $key => $record) {
            $tab[$key]['department'] = $record['name'];
            $tab[$key]['begin'] = date('H:i', strtotime($record['begin']));
            $tab[$key]['end'] = date('H:i', strtotime($record['end']));
            $tab[$key]['km'] = $record['km'];
        }
        return $tab;
    }

    /**
     * dodanie godzin urzytkownikowi
     * czas musi być w formacie Y-m-d H:i:s
     *
     * @param int $idUser            
     * @param DayHours $HoursArray            
     */
    public function addHours($idUser, $Hour)
    {
        
        // TODO: przechwycic błędy z PDO
        $stmt = $this->pdo->prepare('INSERT INTO DayHours (idUser,idDepartment,begin,end,km) 
           VALUES (:idUser,:idDepartment,:begin,:end,:km)');
        
       
        
        $stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);
        $stmt->bindValue(':idDepartment', $Hour->department, PDO::PARAM_INT);
        $stmt->bindValue(':begin', $Hour->begin, PDO::PARAM_STR);
        $stmt->bindValue(':end', $Hour->end, PDO::PARAM_STR);
        $stmt->bindValue(':km', $Hour->km, PDO::PARAM_INT);
        
        $stmt->execute();
    }

    /**
     * Usuwa z danego dnia wszystkie wpisy danego urzytkownika
     * Format daty: rok-miesiac-dzien
     */
    public function deleteDayHours($idUser, $day, $month, $year)
    {
        // $stmt = $this->pdo->prepare('DELETE FROM DayHours
        // WHERE idUser = :idUser AND MONTH(begin) = :month AND YEAR(begin)=:year');
        $stmt = $this->pdo->prepare('DELETE FROM DayHours
           WHERE idUser = :idUser AND DATE(begin) =:date');
        $stmt->bindValue(':idUser', $_SESSION['user_id'], PDO::PARAM_INT);
        $stmt->bindValue(':date', $year . '-' . $month . '-' . $day, PDO::PARAM_INT);
        $stmt->execute();
        // $result=$stmt->fetch();
    }

    /**
     * Zwraca liczbę godzin przpracowanych w danym mieciącu w formacie H:m
     *
     * @param int $idUser            
     * @param int $month            
     * @param int $year            
     * @return string
     */
    public function monthHours($idUser, $month, $year)
    {
        // SELECT sum(((UNIX_TIMESTAMP(end)- UNIX_TIMESTAMP(begin)) /60)/60) FROM `DayHours`
        $stmt = $this->pdo->prepare('SELECT sum(((UNIX_TIMESTAMP(end)- UNIX_TIMESTAMP(begin)) /60))  result FROM DayHours
             WHERE idUser = :idUser AND MONTH(begin) =  :month AND YEAR(begin)=:year');
        
        $stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);
        $stmt->bindValue(':month', $month, PDO::PARAM_STR);
        $stmt->bindValue(':year', $year, PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $min = $result[0]['result'];
        $hours = intval($min / 60);
        $minuts = $min - ($hours * 60);
        return $hours . ':' . $minuts;
    }
    public function monthKm($idUser, $month, $year)
    {
        // SELECT sum(((UNIX_TIMESTAMP(end)- UNIX_TIMESTAMP(begin)) /60)/60) FROM `DayHours`
        $stmt = $this->pdo->prepare('SELECT sum(km) result FROM DayHours
             WHERE idUser = :idUser AND MONTH(begin) =  :month AND YEAR(begin)=:year');
    
        $stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);
        $stmt->bindValue(':month', $month, PDO::PARAM_STR);
        $stmt->bindValue(':year', $year, PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result[0]['result'];
        
    }
}