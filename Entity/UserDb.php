<?php

/**
 * 
 * Klasa reprezentująca urzytkownika w bazie danych
 *
 */
class UserDb extends Db
{

    public $login;

    public $name;

    public $password;

    public $lastName;

    public $id;

    /**
     * wyszukuje urzytkownika o podanym loginie i haśle i zwraca jego id
     * 
     * @return id
     */
    public function loginPassword($login, $password)
    {
        $passwordHash = hash('sha256', $password);
       
        //TODO: przechwycic błędy z PDO
        $stmt = $this->pdo->prepare('SELECT id FROM Users WHERE login = :login AND password = :password');
        
        $stmt->bindValue(':login', $login, PDO::PARAM_STR);
        $stmt->bindValue(':password', $passwordHash, PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($result) > 0)
            return $result[0]['id'];
        else
            return - 1;
    }
}