<?php

/**
 * 
 * Klasa reprezentująca wydzialy firmy w bazie danych
 *
 */
class DepartmentsDb extends Db
{

    public $name;

    public $id;

    /**
     * zwraca wszytkie oddziały
     *
     * @return array
     */
    public function getDepartments()
    {        
        // TODO: przechwycic błędy z PDO
        $stmt = $this->pdo->prepare('SELECT name FROM Departments');
       
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $Dep= array();
        foreach ($result as $temp)
        {
            $Dep[]=$temp['name'];
        }
        return $Dep;
    }
    /**
     * zwraca id departamentu, -1 jesli nie ma takiego departamentu
     * @param string $DepartmentName
     * @return number
     */
    public function getIdDepartment($DepartmentName)
    {
        $stmt = $this->pdo->prepare('SELECT id FROM Departments WHERE name=:name');
        
        $stmt->bindValue(':name', $DepartmentName, PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($result) > 0)
            return $result[0]['id'];
        else
            return - 1;
    }
}