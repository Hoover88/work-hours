<?php

function error404()
{
    $temp = new ErrorsController();
    $temp->error404();
    header("HTTP/1.0 404 Not Found");
}
$uri = $_SERVER["REQUEST_URI"];
$urn = explode('/', $uri);

//wymuszenie logowania jesli nie zalogowany
if (! Session::status()) {
    $temp = new AutenticationController();
    $temp->login();
    exit;
}


switch ($urn[1]) {
    case "login":
        
        if (! Session::status()) {
            $temp = new AutenticationController();
            $temp->login();
        } else {
            header('Location: http://' . $_SERVER['SERVER_NAME']);
        }
        break;
    
    case "logout":
        $temp = new AutenticationController();
        $temp->logout();
        break;
    case "addhours":
        if (count($urn) > 2) { // TODO: zabezpieczyć przed niepopranym formatem daty
            $temp = new MainController();
            $temp->addHours($urn[2]);
            break;
        }
        error404();
        break;
    case "addhoursjson":
        $temp = new MainController();
        $temp->addHoursJon();
        break;
    case "":
        echo "";
        if (Session::status()) {
            $temp = new MainController();
            $temp->main();
            break;
        } else {
            header('Location: http://' . $_SERVER['SERVER_NAME'] . '/login');
            break;
        }
        break;
    case "public":
        readfile($_SERVER["DOCUMENT_ROOT"] . $uri); // TODO: poprawić/zabezpieczyć
        break;
        case "raport": //TODO: zrobić strone raportu
            echo 'strona z raportami jest w trakcie przygotowania';
            break;
    default:
        error404();
        break;
}
?>